import Vue from 'vue';
import Vuex from 'vuex';

import weather from './store/weather'
import location from './store/location'

Vue.use(Vuex);


const state = {
};

export default new Vuex.Store({
  state,
  modules: {
    weather,
    location
  }
});
