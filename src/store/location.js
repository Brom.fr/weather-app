import { getLocation } from "@/api/location";

const state = {
  location: null
};
const mutations = {
  SET_LOCATION(state, { location }) {
    state.location = location;
  }
};
const actions = {
  async getLocation({ commit }, locationQuery) {
    let location = await getLocation(locationQuery)
    commit('SET_LOCATION', { location })
  },

};
export default {
  namespaced: true,
  state,
  mutations,
  actions
};
