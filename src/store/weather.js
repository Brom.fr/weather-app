import {getCurrentCondition} from "@/api/weather";

const state = {
  weather: []
};
const getters = {
  locationWeather: (state) => (locationId) => {
    return state.weather.filter(weather => weather.locationId === locationId)[0]
  }
}
const mutations = {
  SET_WEATHER(state, { weather, locationId }) {
    weather.locationId = locationId
    state.weather.push(weather)
  }
};
const actions = {
  async getWeather({ commit }, locationId) {
    let weather = await getCurrentCondition(locationId)
    commit('SET_WEATHER', { weather, locationId })
  },

};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
