import axiosInstance from "../axios.config";

const GET_LOCATION_PATH = "locations/v1/cities/search";

export const getLocation = async (locationQuery) => {
    return axiosInstance.get(`${GET_LOCATION_PATH}?q=${locationQuery}&language=fr-FR`)
        .then((response) => {
            return response.data;
        }).catch((error) => {
        console.log(error);
    });
}
