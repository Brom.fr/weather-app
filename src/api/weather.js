import axiosInstance from "../axios.config";

const CURRENT_CONDITION_PATH = "/currentconditions/v1";

export const getCurrentCondition = async (locationId) => {

    return axiosInstance.get(`${CURRENT_CONDITION_PATH}/${locationId}?details=true`)
        .then((response) => {
            return response.data[0];
        }).catch((error) => {
        console.log(error);
    });
}
