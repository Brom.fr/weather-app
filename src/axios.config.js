import axios from 'axios';
// Create axios instance
const axiosInstance = axios.create({
    baseURL: process.env.VUE_APP_WEATHER_API_ROOT_URL
});

axiosInstance.interceptors.request.use((config) => {
    config.params = config.params || {};
    config.params['apikey'] = process.env.VUE_APP_WEATHER_API_KEY;
    config.params['language'] = 'fr-FR';
    return config;
});

export default axiosInstance;
